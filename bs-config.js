
module.exports = {
    ui: {
        port: 3001,
    },
    watch: true,
    ignore: [],
    single: false,
    watchOptions: {
        ignoreInitial: true,
    },
    server: 'build',
    port: 4000,
    https: false,
    notify: false,
};
