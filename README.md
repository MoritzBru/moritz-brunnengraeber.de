# moritz-brunnengraeber.de

* https://www.moritz-brunnengraeber.de/
* https://gitlab.com/MoritzBru/moritz-brunnengraeber.de

## Made with...

- [11ty](https://www.11ty.dev/)
- [webpack](https://webpack.js.org/)
- SCSS, autoprefixer, babel, ...
- ❤️ and 🌃

## Dependencies

| Name                                                             | Version (used) | Version (latest)                                                                                                   |
| ---------------------------------------------------------------- | -------------- | ------------------------------------------------------------------------------------------------------------------ |
| [Screenfull](https://github.com/sindresorhus/screenfull.js)      | 3.3.2          | [![CDNJS](https://img.shields.io/cdnjs/v/screenfull.js.svg)](https://cdnjs.com/libraries/screenfull.js)            |
| [Bowser](https://github.com/lancedikson/bowser)                  | 1.9.4          | [![CDNJS](https://img.shields.io/cdnjs/v/bowser.svg)](https://cdnjs.com/libraries/bowser)                          |
| [Marzipano](http://www.marzipano.net/)                           | 0.8.0          | [![npm](https://img.shields.io/npm/v/marzipano)](http://www.marzipano.net/)                                        |
| [Maps JS API](https://developers.google.com/maps/documentation/) | v3             | [v3](https://developers.google.com/maps/documentation/javascript/reference/3.exp/)                                 |
| [reCAPTCHA](https://developers.google.com/recaptcha/intro)       | v3             | [v3](https://developers.google.com/recaptcha/docs/v3)                                                              |
| [EmailJS](https://www.emailjs.com/docs/)                         | v1             | [v1](https://www.emailjs.com/docs/rest-api/send-form/)                                                             |


## TODOs

* check global styles and revise focus styles in nav
* map bg ?
* setup Google Analytics
* refactor Pano
* replace particles js with static svg
* check a11y (Pa11y?, Lighthouse CI?)
* check Performance (Lighthouse CI?)
* support prefers-reduced-motion
* support prefers-color-scheme
* check high contrast mode
* SCSS use @use instead of @import
* adjust scrollbar style
* use :focus-visible (after it is fully supported)
* fade in nav bar bg
