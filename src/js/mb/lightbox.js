import Tobi from '@rqrauhvmra/tobi';

function initLightbox() {
    // eslint-disable-next-line no-unused-vars, no-undef
    const tobi = new Tobi({
        selector: '[data-lightbox]',
        docClose: false,
    });
}

export default initLightbox;
