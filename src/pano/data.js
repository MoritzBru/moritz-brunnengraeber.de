window.APP_DATA = {
    scenes: [
        {
            id: '0-nymphenburg-palace--munich',
            name: 'Nymphenburg Palace | Munich, DE',
            coords: "48°09'30\"N | 11°30'25\"E",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2000,
            initialViewParameters: {
                yaw: 0.04601322573439148,
                pitch: 0.2631856026831265,
                fov: 1.4614289970131633,
            },
            linkHotspots: [],
            infoHotspots: [],
        },
        {
            id: '1-huber-see--penzberg',
            name: 'Huber See | Penzberg, DE',
            coords: "47°45'48\"N | 11°21'06\"E",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2000,
            initialViewParameters: {
                yaw: -1.030449390793958,
                pitch: 0.4357863904535737,
                fov: 1.4614289970131633,
            },
            linkHotspots: [],
            infoHotspots: [],
        },
        {
            id: '2-beach--lido-di-jesolo',
            name: 'Beach  of Lido di Jesolo | Lido di Jesolo, IT',
            coords: "45°30'50\"N | 12°40'33\"E",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2000,
            initialViewParameters: {
                pitch: 0,
                yaw: 0,
                fov: 1.5707963267948966,
            },
            linkHotspots: [],
            infoHotspots: [],
        },
        {
            id: '3-kochelsee--kochel-am-see',
            name: 'Kochelsee | Kochel am See, DE',
            coords: "47°37'46\"N | 11°22'37\"E",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2000,
            initialViewParameters: {
                yaw: -1.8313375505664062,
                pitch: 0.28187925365698163,
                fov: 1.4614289970131633,
            },
            linkHotspots: [],
            infoHotspots: [],
        },
        {
            id: '4-gut-schwaig--iffeldorf',
            name: 'Gut Schwaig | Iffeldorf, DE',
            coords: "47°46'52\"N | 11°17'47\"E",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2000,
            initialViewParameters: {
                yaw: -0.3712791317878832,
                pitch: 0.38300994211619255,
                fov: 1.4614289970131633,
            },
            linkHotspots: [],
            infoHotspots: [],
        },
        {
            id: '5-fairground--olching',
            name: 'Fairground Olching | Olching, DE',
            coords: "48°12'33\"N | 11°19'23\"E",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2000,
            initialViewParameters: {
                yaw: -1.4937037708602965,
                pitch: 0.7315982403742822,
                fov: 1.4614289970131633,
            },
            linkHotspots: [],
            infoHotspots: [],
        },
        {
            id: '6-lac-de-lospedale--porto-veccio',
            name: "Lac de L'Ospedale | Porto Veccio, FR",
            coords: "41°39'42\"N | 9°11'47\"E",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2000,
            initialViewParameters: {
                yaw: -1.9000721694652185,
                pitch: 0.257230615583266,
                fov: 1.4614289970131633,
            },
            linkHotspots: [],
            infoHotspots: [],
        },
        {
            id: '7-aubinger-lohe--munich',
            name: 'Aubinger Lohe | Munich, DE',
            coords: "48°10'15\"N | 11°23'58\"E",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2000,
            initialViewParameters: {
                yaw: -2.6717276222336253,
                pitch: 0.7622362687171069,
                fov: 1.4614289970131633,
            },
            linkHotspots: [],
            infoHotspots: [],
        },
    ],
    name: 'Panoramen',
    settings: {
        mouseViewMode: 'drag',
        autorotateEnabled: true,
        fullscreenButton: true,
        viewControlButtons: false,
    },
};
